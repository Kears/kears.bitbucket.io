# Writing Portfolio


## Guide to Parmigiano-Reggiano

[Document](https://bitbucket.org/Kears/portfolio/src/master/parmigiano-reggiano-guide.md)  

Intended for a non-technical audience. A guide to purchasing, storing and
preparing the Italian cheese Parmigiano-Reggiano, commonly called parmesan in
the United States. Written in Markdown. Includes how to identify authentic
parmigiano from its imitations. 


## Accordion Widget

[Live Sample](https://kears.bitbucket.io/accordion/)  
[Source Code](https://bitbucket.org/Kears/kears.bitbucket.io/src/master/accordion/)  

I used HTML, CSS and JavaScript but no libraries or frameworks to build 
this accordion widget. An [accordion] is a graphical user interface component
comprised of a vertically stacked list of items. Tapping on an item expands it
to reveal additional information about that item. My accordion widget uses
CSS to expand and collapse accordion items. The widget uses JavaScript to 
attach an event handler for mouse and touch events. The event handler toggles
CSS classes that then expand or collapse accordion items. 

[accordion]:https://jqueryui.com/accordion/


## PowerShell Setup & Configuration Guide

[Document](https://bitbucket.org/Kears/portfolio/src/master/powershell-setup-config-guide.md)  

Intended for a moderately technical audience. A guide to setup and configure
PowerShell for Windows 10. Written in Markdown with manually-verified code
samples. 


## Python Object to JSON

[Repository Home](https://bitbucket.org/Kears/python-object-to-json/)  

Intended for a highly technical audience. A Git repository for a Python
library with API documentation and code samples. I authored the repository,
the source code for the library itself, the unit tests for the source code,
the reference documentation, the code samples, and the unit tests that verify
the code samples. 
